import axios from 'axios'
import EventBus from './event-bus'

export default {
  data () {
    return {
      events: {
        get: {
          user: 'get-user',
          items: 'get-items',
          categories: 'get-categories',
          ingredients: 'get-ingredients',
          units: 'get-units',
          recipes: 'get-recipes',
          recipe: 'get-recipe',
          thermomixModels: 'get-thermomix-models',
          users: 'get-users'
        },
        change: {
          categories: 'change-categories'
        },
        edit: {
          categories: 'edit-categories',
          ingredient: 'edit-ingredient',
          unit: 'edit-unit',
          thermomixModels: 'edit-thermomix-models'
        },
        reload: {
          recipe: 'reload-recipe',
          user: 'reload-user'
        },
        delete: {
          recipe: 'delete-recipe',
          ingredient: 'delete-ingredient',
          step: 'delete-step'
        },
        save: {
          recipe: 'save-recipe',
          ingredient: 'save-ingredient',
          step: 'save-step'
        },
        upload: {
          photo: 'upload-photo'
        },
        registered: 'registered'
      },
      selects: {
        categories: []
      },
      data: {
        recipe: {}
      }
    }
  },
  methods: {
    register (data) {
      this.post('/v1/users/register', data, null, this.events.registered)
    },

    me (userId) {
      this.get('/v1/users/profile/me' + (userId ? '/' + userId : ''), {}, this.events.get.user)
    },

    recipes (userId, filter) {
      if (typeof filter !== 'undefined') {
        filter.options = {
          orderBy: {
            field: 'name',
            order: 'ASC'
          }
        }
      } else {
        filter = {
          options: {
            orderBy: {
              field: 'name',
              order: 'ASC'
            }
          }
        }
      }

      this.get('/v1/users/profile/recipes' + (userId ? '/' + userId : ''), filter, this.events.get.recipes)
    },

    recipe (id) {
      this.get('/v1/users/profile/recipes/get/' + id, {}, this.events.get.recipe)
    },

    recipeSlug (slug) {
      this.get('/v1/users/profile/recipes/view/' + slug, {}, this.events.get.recipe)
    },

    categories () {
      this.get('/v1/categories', {
        options: {
          orderBy: {
            field: 'name',
            order: 'ASC'
          }
        }
      }, this.events.get.categories)
    },

    ingredients (filter) {
      filter.options = {
        orderBy: {
          field: 'name',
          order: 'ASC'
        }
      }

      this.get('/v1/ingredients', filter, this.events.get.ingredients)
    },

    units () {
      this.get('/v1/units', {
        options: {
          orderBy: {
            field: 'name',
            order: 'ASC'
          }
        }
      }, this.events.get.units)
    },

    thermomixModels () {
      this.get('/v1/thermomix/models', {
        options: {
          orderBy: {
            field: 'name',
            order: 'ASC'
          }
        }
      }, this.events.get.thermomixModels)
    },

    users (filter) {
      if (typeof filter !== 'undefined') {
        filter.options = {
          orderBy: {
            field: 'name',
            order: 'ASC'
          }
        }
      } else {
        filter = {
          options: {
            orderBy: {
              field: 'name',
              order: 'ASC'
            }
          }
        }
      }

      this.get('/v1/users', filter, this.events.get.users)
    },

    saveRecipe (params) {
      if (params.id) {
        this.post('/v1/users/profile/recipes/update/' + params.id, params, 'Recepta guardada correctament!')
      } else {
        this.post('/v1/users/profile/recipes/create', params, 'Recepta guardada correctament!', '/recipes/edit/')
      }
    },

    savePhoto (photo, recipe) {
      const formData = new FormData()
      console.log(photo)
      formData.append('photo', photo, photo.name)

      this.upload('/v1/users/profile/recipes/upload/' + recipe.id, formData, 'Foto guardada correctament!', null, this.events.upload.photo)
    },

    saveIngredient (recipe, ingredient) {
      this.data.recipe = recipe
      let url = ''
      if (ingredient.id) {
        url = '/v1/users/profile/recipes/' + recipe.id + '/ingredients/update/' + ingredient.id
        ingredient.editable = false
      } else {
        url = '/v1/users/profile/recipes/' + recipe.id + '/ingredients/create'
      }
      this.post(
        url,
        ingredient,
        ingredient.id ? 'Ingredient actualizat correctament!' : 'Ingredient afegit correctament!',
        null,
        this.events.save.ingredient
      )
    },

    saveStep (recipe, step) {
      this.data.recipe = recipe
      let url = ''
      if (step.id) {
        url = '/v1/users/profile/recipes/' + recipe.id + '/steps/update/' + step.id
        step.editable = false
      } else {
        url = '/v1/users/profile/recipes/' + recipe.id + '/steps/create'
      }

      this.post(
        url,
        step,
        step.id ? 'Pas actualitzat correctament!' : 'Pas afegit correctament!',
        null,
        this.events.save.step
      )
    },

    updatePositionSteps (recipe, steps) {
      this.data.recipe = recipe
      this.post(
        '/v1/users/profile/recipes/' + recipe.id + '/steps/refresh',
        { steps: steps },
        null,
        null,
        this.events.reload.recipe
      )
    },

    saveFavorite (recipe) {
      this.data.recipe = recipe
      this.post(
        '/v1/users/profile/favorites/add/' + recipe.id,
        {},
        null,
        null,
        this.events.reload.user
      )
    },

    get (url, params, event) {
      if (typeof event === 'undefined') {
        event = this.events.get.items
      }
      axios.get(process.env.VUE_APP_API_URL + url, {
        params: params,
        headers: {
          Authorization: this.$store.getters.token
        }
      }).then(resp => {
        EventBus.$emit(event, resp.data.data)
      }).catch(err => {
        console.log(err)
        this.checkToken(err)
        this.$bvToast.toast(err.response.data.errors[0].message, {
          title: 'Error!',
          variant: 'danger',
          class: 'position-fixed fixed-top m-0 rounded-0',
          solid: true,
          autoHideDelay: 5000
        })
      })
    },

    post (url, params, message, urlCallback, event) {
      axios.post(process.env.VUE_APP_API_URL + url, params, {
        headers: {
          Authorization: this.$store.getters.token
        }
      }).then(resp => {
        this.$bvToast.toast(message, {
          title: 'Fet!',
          variant: 'success',
          class: 'position-fixed fixed-top m-0 rounded-0',
          solid: true,
          autoHideDelay: 5000
        })

        if (typeof urlCallback !== 'undefined' && urlCallback) {
          window.location.href = urlCallback + resp.data.data.id
        }

        if (typeof event !== 'undefined' && event) {
          EventBus.$emit(event, this.data)
        }
      }).catch(err => {
        console.log(err)
        this.checkToken(err)
        this.$bvToast.toast(err.response.data.errors[0].message, {
          title: 'Error!',
          variant: 'danger',
          class: 'position-fixed fixed-top m-0 rounded-0',
          solid: true,
          autoHideDelay: 5000
        })
      })
    },

    upload (url, params, message, urlCallback, event) {
      axios.post(process.env.VUE_APP_API_URL + url, params, {
        headers: {
          Authorization: this.$store.getters.token,
          'Content-Type': 'multipart/form-data'
        }
      }).then(resp => {
        this.$bvToast.toast(message, {
          title: 'Fet!',
          variant: 'success',
          class: 'position-fixed fixed-top m-0 rounded-0',
          solid: true,
          autoHideDelay: 5000
        })

        if (typeof urlCallback !== 'undefined' && urlCallback) {
          window.location.href = urlCallback + resp.data.data.id
        }

        if (typeof event !== 'undefined' && event) {
          EventBus.$emit(event, this.data)
        }
      }).catch(err => {
        console.log(err)
        this.checkToken(err)
        this.$bvToast.toast(err.response.data.errors[0].message, {
          title: 'Error!',
          variant: 'danger',
          class: 'position-fixed fixed-top m-0 rounded-0',
          solid: true,
          autoHideDelay: 5000
        })
      })
    },

    delete (url, obj, messages, event) {
      if (messages) {
        this.$confirm({
          title: 'Atenció!',
          message: messages.question,
          button: {
            yes: 'confirmar',
            no: 'cancelar'
          },
          callback: confirm => {
            if (confirm) {
              axios
                .post(process.env.VUE_APP_API_URL + url, {}, {
                  headers: {
                    Authorization: this.$store.getters.token
                  }
                })
                .then((resp) => {
                  this.$confirm({
                    title: 'Atenció!',
                    message: messages.deleted,
                    button: {
                      yes: 'confirmar'
                    },
                    callback: confirm => {
                      EventBus.$emit(event, resp.data.data)
                    }
                  })
                })
            }
          }
        })
      } else {
        axios
          .post(process.env.VUE_APP_API_URL + url, {}, {
            headers: {
              Authorization: this.$store.getters.token
            }
          })
          .then((resp) => {
            EventBus.$emit(event, resp.data.data)
          })
      }
    },

    checkToken (err) {
      if (typeof err.response.data.error !== 'undefined' &&
        (err.response.data.error === 'TOKEN_INVALID' || err.response.data.error === 'TOKEN_EXPIRED' || err.response.data.error === 'TOKEN_NOT_FOUND')) {
        this.$store.dispatch('logout')
          .then(() => {
            window.location.href = '/'
          })
      }
    },

    scrollToEnd () {
      setTimeout(function () {
        window.scrollTo(0, document.documentElement.scrollHeight)
      }, 500)
    }
  }
}
