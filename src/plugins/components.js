import DropdownCategories from '@/components/dropdowns/categories'
import DropdownIngredients from '@/components/dropdowns/ingredients'
import DropdownUnits from '@/components/dropdowns/units'
import CheckboxCategories from '@/components/checkbox/categories'
import CheckboxThermomixModels from '@/components/checkbox/thermomix-models'
import RecipeList from '@/components/recipes/list'
import draggable from 'vuedraggable'

export default {
  components: {
    'dropdown-categories': DropdownCategories,
    'dropdown-ingredients': DropdownIngredients,
    'dropdown-units': DropdownUnits,
    'checkbox-categories': CheckboxCategories,
    'checkbox-thermomix-models': CheckboxThermomixModels,
    'recipes-list': RecipeList,
    draggable
  }
}
