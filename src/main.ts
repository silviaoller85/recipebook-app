import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import {
  NavbarPlugin, NavPlugin, ToastPlugin, CardPlugin,
  FormSelectPlugin, FormInputPlugin, FormTextareaPlugin, ButtonPlugin, FormRadioPlugin,
  FormGroupPlugin, InputGroupPlugin, FormCheckboxPlugin, ListGroupPlugin, TabsPlugin, ModalPlugin, FormFilePlugin
} from 'bootstrap-vue'
import VueConfirmDialog from 'vue-confirm-dialog'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faHourglassHalf, faUsers, faHeart, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUser, faHourglassHalf, faUsers, faHeart, faInfoCircle)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.use(NavbarPlugin)
Vue.use(NavPlugin)
Vue.use(ToastPlugin)
Vue.use(CardPlugin)
Vue.use(ListGroupPlugin)
Vue.use(FormSelectPlugin)
Vue.use(FormInputPlugin)
Vue.use(FormRadioPlugin)
Vue.use(FormGroupPlugin)
Vue.use(FormTextareaPlugin)
Vue.use(ButtonPlugin)
Vue.use(InputGroupPlugin)
Vue.use(FormCheckboxPlugin)
Vue.use(TabsPlugin)
Vue.use(ModalPlugin)
Vue.use(FormFilePlugin)

Vue.use(VueConfirmDialog)
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
