import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from '@/store/index'
import Login from '@/views/login.vue'
import Register from '@/views/register.vue'
import RecipeList from '@/views/recipes/list.vue'
import RecipeEdit from '@/views/recipes/edit.vue'
import RecipeView from '@/views/recipes/view.vue'
import UserView from '@/views/users/view.vue'
import FavoritesRecipe from '@/views/favorites/recipes.vue'
import Search from '@/views/recipes/search.vue'
import Sitemap from '@/views/sitemap.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      title: 'Recipe Book | Login'
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      title: 'Recipe Book | Registra\'t'
    }
  },
  {
    path: '/',
    name: 'Home',
    component: RecipeList,
    meta: {
      title: 'Recipe Book | Llistat de receptes'
    }
  },
  {
    path: '/recipes/new',
    name: 'Nova recepta',
    component: RecipeEdit,
    meta: {
      requiresAuth: true,
      title: 'Recipe Book | Nova recepta'
    }
  },
  {
    path: '/recipes/edit/:id',
    name: 'Modificar la recepta',
    component: RecipeEdit,
    meta: {
      requiresAuth: true,
      title: 'Recipe Book | Editar recepta'
    }
  },
  {
    path: '/recipes/view/:slug',
    name: 'Recepta',
    component: RecipeView,
    meta: {
      title: 'Recipe Book'
    }
  },
  {
    path: '/users/:id',
    name: 'Usuari',
    component: UserView,
    meta: {
      title: 'Recipe Book | Usuari'
    }
  },
  {
    path: '/favorites',
    name: 'Favorites',
    component: FavoritesRecipe,
    meta: {
      requiresAuth: true,
      title: 'Recipe Book | Les teves receptes preferides'
    }
  },
  {
    path: '/search',
    name: 'Search',
    component: Search,
    meta: {
      title: 'Recipe Book | Buscador de receptes'
    }
  },
  {
    path: '/sitemap',
    name: 'Sitemap',
    component: Sitemap
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name === 'Login' && store.getters.isLoggedIn) {
    next('/')
    return
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  }

  next()
})

export default router
