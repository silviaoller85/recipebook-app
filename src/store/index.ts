import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import EventBus from '@/plugins/event-bus'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token'),
    user: localStorage.getItem('user')
  },
  mutations: {
    authRequest (state) {
      state.status = 'loading'
    },
    authSuccess (state, { token, user }) {
      state.status = 'success'
      state.token = token
      state.user = user
    },
    authError (state) {
      state.status = 'error'
    },
    logout (state) {
      state.status = ''
      state.user = ''
      state.token = ''
    }
  },
  actions: {
    login ({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('authRequest')
        axios.post(process.env.VUE_APP_API_URL + '/v1/users/login', {
          email: user.email,
          password: user.password
        },
        {
          headers: {
            Authorization: process.env.VUE_APP_API_TOKEN
          }
        })
          .then(resp => {
            const token = resp.data.token_type + ' ' + resp.data.access_token
            const user = JSON.stringify(resp.data.data)
            localStorage.setItem('token', token)
            localStorage.setItem('user', user)
            commit('authSuccess', { token, user })
            window.location.href = '/'
          })
          .catch(err => {
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            commit('authError')
            reject(err)
          })
      })
    },
    logout ({ commit }) {
      return new Promise((resolve, reject) => {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        commit('logout')
        resolve()
      })
    },
    refresh ({ commit }) {
      return new Promise((resolve, reject) => {
        commit('authRequest')
        const token = localStorage.getItem('token')
        axios.get(process.env.VUE_APP_API_URL + '/v1/users/profile/me',
          {
            headers: {
              Authorization: token
            }
          })
          .then(resp => {
            const user = JSON.stringify(resp.data.data)
            localStorage.setItem('user', user)
          })
          .catch(err => {
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            commit('authError')
            reject(err)
          })
      })
    }
  },
  modules: {

  },
  getters: {
    isLoggedIn (state) {
      return state.token ? state.token : null
    },
    authStatus (state) {
      return state.status
    },
    user (state) {
      return state.user ? JSON.parse(String(state.user)) : null
    },
    token (state) {
      return state.token
    }
  }
})
